<?php
declare(strict_types=1);

namespace App\Validators;

use InvalidArgumentException;

interface ValidatorInterface
{
    /**
     * Validates given data.
     *
     * @param mixed $data
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function validate($data): void;
}
