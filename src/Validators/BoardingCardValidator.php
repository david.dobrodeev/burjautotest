<?php
declare(strict_types=1);

namespace App\Validators;

use App\Interfaces\TransportTypes;
use InvalidArgumentException;

final class BoardingCardValidator implements ValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate($data): void
    {
        if (empty($data['transport']) || empty($data['from']) || empty($data['to'])) {
            throw new InvalidArgumentException('`transport` or `from` or `to` must not be empty');
        }

        if (\in_array($data['transport'], TransportTypes::ALL_TYPES, true) === false) {
            throw new InvalidArgumentException('unknown trasport type');
        }

        if ($data['transport'] === TransportTypes::FLIGHT) {
            if (empty($data['gate']) || empty($data['seat'])) {
                throw new InvalidArgumentException('`gate` or `seat` must not be empty');
            }
        }

        if ($data['transport'] === TransportTypes::TRAIN) {
            if (empty($data['seat'])) {
                throw new InvalidArgumentException('`seat` must not be empty');
            }
        }
    }
}
