<?php
declare(strict_types=1);

namespace App\Factories;

use App\Cards\AbstractBoardingCard;

interface FactoryInterface
{
    /**
     * Creates instance of BoardingCard.
     *
     * @param mixed $data
     *
     * @return \App\Cards\AbstractBoardingCard
     *
     * @throws \Exception
     */
    public function create($data): AbstractBoardingCard;
}
