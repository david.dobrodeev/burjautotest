<?php
declare(strict_types=1);

namespace App\Factories;

use App\Cards\AbstractBoardingCard;
use App\Cards\BusBoardingCard;
use App\Cards\FlightBoardingCard;
use App\Cards\TrainBoardingCard;
use App\Interfaces\TransportTypes;
use Exception;

final class BoardingCardFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create($data): AbstractBoardingCard
    {
        if (TransportTypes::FLIGHT === $data['transport']) {
            return $this->createFlightCard($data);
        }

        if (TransportTypes::TRAIN === $data['transport']) {
            return $this->createTrainCard($data);
        }

        if (TransportTypes::AIRPORT_BUS === $data['transport']) {
            return $this->createAirportBusCard($data);
        }

        throw new Exception('Can not create boarding card');
    }

    /**
     * Creates AirportBusCard.
     *
     * @param mixed[] $data
     *
     * @return \App\Cards\AbstractBoardingCard
     */
    private function createAirportBusCard($data): AbstractBoardingCard
    {
        return
            (new BusBoardingCard())
                ->setDetails($data['details'] ?? '')
                ->setFrom($data['from'])
                ->setTo($data['to']);
    }

    /**
     * Creates FlightCard.
     *
     * @param mixed[] $data
     *
     * @return \App\Cards\AbstractBoardingCard
     */
    private function createFlightCard(array $data): AbstractBoardingCard
    {
        return
            (new FlightBoardingCard())
                ->setNumber($data['number'])
                ->setDetails($data['details'] ?? '')
                ->setFrom($data['from'])
                ->setTo($data['to'])
                ->setGate((int) $data['gate'])
                ->setSeat($data['seat']);
    }

    /**
     * Creates TrainCard.
     *
     * @param mixed[] $data
     *
     * @return \App\Cards\AbstractBoardingCard
     */
    private function createTrainCard(array $data): AbstractBoardingCard
    {
        return
            (new TrainBoardingCard())
                ->setDetails($data['details'] ?? '')
                ->setNumber($data['number'] ?? '')
                ->setFrom($data['from'])
                ->setTo($data['to'])
                ->setSeat($data['seat']);
    }
}
