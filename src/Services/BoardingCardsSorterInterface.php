<?php
declare(strict_types=1);

namespace App\Services;

use App\Cards\AbstractBoardingCard;

interface BoardingCardsSorterInterface
{
    /**
     * Sorts Boarding cards.
     *
     * @param AbstractBoardingCard[] $cards
     *
     * @return AbstractBoardingCard[]
     */
    public function sort(array $cards): array;
}
