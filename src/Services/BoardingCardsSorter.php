<?php
declare(strict_types=1);

namespace App\Services;

final class BoardingCardsSorter implements BoardingCardsSorterInterface
{
    /**
     * {@inheritdoc}
     */
    public function sort(array $cards): array
    {
        [$froms, $tos] = $this->getFromListAndToList($cards);

        $firstNode = \reset($cards);
        $result[] = $firstNode;
        $nextNode = $firstNode->getTo();
        $prevNode = $firstNode->getFrom();

        while (true) {
           $break = true;
           if (isset($froms[$nextNode])) {
               $nextItem = $froms[$nextNode];
               $result[] = $nextItem;
               $nextNode = $nextItem->getTo();
               $break = false;
           }
           if (isset($tos[$prevNode])) {
               $prevItem = $tos[$prevNode];
               \array_unshift($result, $prevItem);
               $prevNode = $prevItem->getFrom();
               $break = false;
           }

           if ($break) {
               break;
           }
        }

        return $result;
    }

    /**
     * Get `from` list and `to` list from cards.
     *
     * @param \App\Cards\AbstractBoardingCard[] $boardingCards
     *
     * @return mixed[]
     */
    private function getFromListAndToList(array $boardingCards): array
    {
        $from = [];
        $tos = [];

        foreach ($boardingCards as $boardingCard) {
            $from[$boardingCard->getFrom()] = $boardingCard;
            $tos[$boardingCard->getTo()] = $boardingCard;
        }

        return [$from, $tos];
    }
}
