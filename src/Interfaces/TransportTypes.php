<?php
declare(strict_types=1);

namespace App\Interfaces;

interface TransportTypes
{
    /**
     * @var string[]
     */
    public const ALL_TYPES = [
        self::FLIGHT,
        self::AIRPORT_BUS,
        self::TRAIN,
    ];

    /**
     * @var string
     */
    public const FLIGHT = 'flight';

    /**
     * @var string
     */
    public const AIRPORT_BUS = 'airport bus';

    /**
     * @var string
     */
    public const TRAIN = 'train';
}
