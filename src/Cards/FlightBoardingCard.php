<?php
declare(strict_types=1);

namespace App\Cards;

use App\Interfaces\TransportTypes;

final class FlightBoardingCard extends AbstractBoardingCard implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @var int
     */
    protected $gate;

    /**
     * @var string
     */
    protected $seat;

    /**
     * AirportBoardingCard constructor.
     */
    public function __construct()
    {
        $this->transport = TransportTypes::FLIGHT;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     *
     * @return self
     */
    public function setNumber(?string $number = null): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getGate(): ?int
    {
        return $this->gate;
    }

    /**
     * @param int|null $gate
     *
     * @return self
     */
    public function setGate(?int $gate = null): self
    {
        $this->gate = $gate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * @param string|null $seat
     *
     * @return self
     */
    public function setSeat(?string $seat = null): self
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return \sprintf(
            'From %s Airport, take flight %s to %s. Gate %s, seat %s. %s',
            $this->from,
            $this->number,
            $this->to,
            $this->gate,
            $this->seat,
            $this->details
        );
    }
}
