<?php
declare(strict_types=1);

namespace App\Cards;

use App\Interfaces\TransportTypes;

final class TrainBoardingCard extends AbstractBoardingCard implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $seat;

    /**
     * TrainBoardingCard constructor.
     */
    public function __construct()
    {
        $this->transport = TransportTypes::TRAIN;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     *
     * @return self
     */
    public function setNumber(?string $number = null): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * @param string|null $seat
     *
     * @return self
     */
    public function setSeat(?string $seat = null): self
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return \sprintf(
            'Take train %s from %s to %s. Sit in seat %s. %s',
            $this->number,
            $this->from,
            $this->to,
            $this->seat,
            $this->details
        );
    }
}
