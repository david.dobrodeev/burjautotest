<?php
declare(strict_types=1);

namespace App\Cards;

use App\Interfaces\TransportTypes;

final class BusBoardingCard extends AbstractBoardingCard implements \JsonSerializable
{
    /**
     * BusBoardingCard constructor.
     */
    public function __construct()
    {
        $this->transport = TransportTypes::AIRPORT_BUS;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return \sprintf(
            'Take the airport bus from %s to %s Airport. No seat assignment. %s',
            $this->from,
            $this->to,
            $this->details
        );
    }
}
