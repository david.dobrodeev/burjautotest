<?php
declare(strict_types=1);

namespace App\Cards;

abstract class AbstractBoardingCard
{
    /**
     * @var string
     */
    protected $transport;

    /**
     * @var string
     */
    protected $from;

    /**
     * @var string
     */
    protected $to;

    /**
     * @var string
     */
    protected $details;

    /**
     * @return string|null
     */
    public function getTransport(): ?string
    {
        return $this->transport;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @param string|null $from
     *
     * @return self
     */
    public function setFrom(?string $from = null): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @param string|null $to
     *
     * @return self
     */
    public function setTo(?string $to = null): self
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetails(): ?string
    {
        return $this->details;
    }

    /**
     * @param string|null $details
     *
     * @return self
     */
    public function setDetails(?string $details = null): self
    {
        $this->details = $details;

        return $this;
    }
}
