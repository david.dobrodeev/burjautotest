<?php

use App\Factories\BoardingCardFactory;
use App\Services\BoardingCardsSorter;
use App\Validators\BoardingCardValidator;

$basePath = \realpath(\dirname(__DIR__));
require_once $basePath . '/vendor/autoload.php';

$postData = \file_get_contents('php://input');
$data = \json_decode($postData, true, 512, JSON_THROW_ON_ERROR);

$validator = new BoardingCardValidator();
$boardingCardFactory = new BoardingCardFactory();
$boardingCardSorter = new BoardingCardsSorter();

$cards = [];

foreach ($data as $item) {
    $validator->validate($item);
    $cards[] = $boardingCardFactory->create($item);
}

$boardingCards = $boardingCardSorter->sort($cards);
$boardingCards[] = 'You have arrived at your final destination.';

echo \json_encode($boardingCards, JSON_THROW_ON_ERROR, 512);

