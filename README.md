###Instalation
```
git clone https://gitlab.com/david.dobrodeev/burjautotest.git burjauto
cd burjauto
composer install
cd public
php -S localhost:8000
```

### Example Request
```
POST localhost:8000

Body
[
	{
		"transport": "flight",
		"number": "SK456",
		"from": "Destination1",
		"to": "Destination2",
		"seat": "3A",
		"gate": "45B",
		"details": "Baggage drop at ticket counter 344"
	},
	{
		"transport" : "airport bus",
		"from": "Destination2",
		"to": "Destination3"
	},
	{
		"transport": "flight",
		"number": "SK422",
		"from": "Destination3",
		"to": "Destination4",
		"seat": "7B",
		"gate": "22",
		"details": "Baggage will we automatically transferred from your last leg."
	},
	{
		"transport" : "train",
		"number": "78A",
		"from": "Destination4",
		"to": "Destination5",
		"seat": "45B"
	}
]
```

### Example Response
```
[
    "Take train 78A from Destination1 to Destination2. Sit in seat 45B. ",
    "Take the airport bus from Destination2 to Destination3 Airport. No seat assignment. ",
    "From Destination3 Airport, take flight SK456 to Destination4. Gate 45, seat 3A. Baggage drop at ticket counter 344",
    "From Destination4 Airport, take flight SK422 to Destination5. Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.",
    "You have arrived at your final destination."
]
```

###Testing
```
vendor/bin/phpunit test
```
