<?php
declare(strict_types=1);

namespace App\Test\Services;

use App\Cards\BusBoardingCard;
use App\Cards\FlightBoardingCard;
use App\Cards\TrainBoardingCard;
use App\Services\BoardingCardsSorter;
use PHPUnit\Framework\TestCase;

final class BoardingCardsSorterTest extends TestCase
{
    /**
     * Tests sorting succeeds.
     *
     * @return void
     */
    public function testSortSucceeds(): void
    {
        $boardingCards = $this->createSortedBoardingCards();
        $cardsToShuffle = $boardingCards;
        \shuffle($cardsToShuffle);
        $boardingCardSorter = new BoardingCardsSorter();

        $actualResult = $boardingCardSorter->sort($cardsToShuffle);

        self::assertSame($boardingCards, $actualResult);
    }

    /**
     * Tests sorting withbroken chain.
     *
     * @return void
     */
    public function testSortWithBrokenChain(): void
    {
        $boardingCards = $this->createSortedBoardingCardsWithBrokenChain();
        $cardsToShuffle = $boardingCards;
        \shuffle($cardsToShuffle);
        $boardingCardSorter = new BoardingCardsSorter();

        $actualResult = $boardingCardSorter->sort($cardsToShuffle);

        self::assertSame([$boardingCards[0], $boardingCards[1], $boardingCards[2]], $actualResult);
    }

    /**
     * Creates sorted AbstractBoardingCards
     *
     * @return \App\Cards\AbstractBoardingCard[]
     */
    private function createSortedBoardingCards(): array
    {
        $point1 =
            (new BusBoardingCard())
                ->setFrom('Destonation1')
                ->setTo('Destonation2');
        $point2 =
            (new FlightBoardingCard())
                ->setNumber('SK22')
                ->setFrom('Destonation2')
                ->setTo('Destonation3')
                ->setGate(22)
                ->setSeat('3A')
                ->setDetails('details1');
        $point3 =
            (new FlightBoardingCard())
                ->setNumber('SK23')
                ->setFrom('Destonation3')
                ->setTo('Destonation4')
                ->setGate(23)
                ->setSeat('3A')
                ->setDetails('details2');
        $point4 =
            (new BusBoardingCard())
                ->setFrom('Destonation4')
                ->setTo('Destonation5');
        $point5 =
            (new TrainBoardingCard())
                ->setNumber('Train-1')
                ->setFrom('Destonation5')
                ->setTo('Destonation6')
                ->setSeat('3A')
                ->setDetails('details3');

        return [
            $point1,
            $point2,
            $point3,
            $point4,
            $point5,
        ];
    }

    /**
     * Creates sorted AbstractBoardingCards with broken chain
     *
     * @return \App\Cards\AbstractBoardingCard[]
     */
    private function createSortedBoardingCardsWithBrokenChain(): array
    {
        $point1 =
            (new BusBoardingCard())
                ->setFrom('Destonation1')
                ->setTo('Destonation2');
        $point2 =
            (new FlightBoardingCard())
                ->setNumber('SK22')
                ->setFrom('Destonation2')
                ->setTo('Destonation3')
                ->setGate(22)
                ->setSeat('3A')
                ->setDetails('details1');
        $point3 =
            (new FlightBoardingCard())
                ->setNumber('SK23')
                ->setFrom('Destonation3')
                ->setTo('Destonation4')
                ->setGate(23)
                ->setSeat('3A')
                ->setDetails('details2');
        $point5 =
            (new TrainBoardingCard())
                ->setNumber('Train-1')
                ->setFrom('Destonation5')
                ->setTo('Destonation6')
                ->setSeat('3A')
                ->setDetails('details3');

        return [
            $point1,
            $point2,
            $point3,
            $point5,
        ];
    }
}
