<?php
declare(strict_types=1);

namespace App\Test\Cards;

use App\Cards\FlightBoardingCard;
use PHPUnit\Framework\TestCase;

final class FlightBoardingCardTest extends TestCase
{
    /**
     * Test converting to json.
     *
     * @return void
     *
     * @throws \JsonException
     */
    public function testConvertingToJson(): void
    {
        $flightBoardingCard =
            (new FlightBoardingCard())
                ->setNumber('SK22')
                ->setFrom('Moscow')
                ->setTo('Dubai')
                ->setGate(22)
                ->setSeat('3A')
                ->setDetails('details');

        $actualResult = \json_encode($flightBoardingCard, \JSON_THROW_ON_ERROR, 512);

        self::assertSame(
            '"From Moscow Airport, take flight SK22 to Dubai. Gate 22, seat 3A. details"',
            $actualResult
        );
    }
}
